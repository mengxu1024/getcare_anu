1. Please install Python 3.8.3 (Latest) first.

2. Then run the “getcare_anu\Django\GetCare\Scripts\Activate" to activate the Virtual environment of Python: $GetCare\Scripts\Activate

3. Install Django 3.1 (Latest) under the Virtual Environment: $pip install Django

4. Run the Server: python GetCare\Project\website\manage.py runserver