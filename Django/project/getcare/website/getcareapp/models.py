from django.db import models
from django.utils import timezone

class Provider(models.Model):
    firstName = models.CharField(max_length=50)
    lastName = models.CharField(max_length=50)
    gender = models.CharField(max_length=1)
    password = models.CharField(max_length=128)
    jobType = models.CharField(max_length=15)  #CARE\MEDICATION\COMMUNICATION\ACTIVITY\MEAL\OTHER
    email = models.CharField(max_length=128)
    phone = models.CharField(max_length=13)
    key = models.CharField(max_length=62)
    relation_code = models.CharField(max_length=10, default='1234567890')

    def __str__(self):
        return self.firstName + " " +self.lastName

class User(models.Model):
    firstName = models.CharField(max_length=50)
    lastName = models.CharField(max_length=50)
    gender = models.CharField(max_length=1)
    password = models.CharField(max_length=128)
    email = models.CharField(max_length=128)
    phone = models.CharField(max_length=13)
    key = models.CharField(max_length=62) #62-bit key used to verify user login
    carer = models.ForeignKey("Carer",on_delete=models.CASCADE,related_name='+', null =True)
    # Providers
    careProvider = models.ForeignKey(Provider, on_delete=models.CASCADE,related_name='+', null = True)
    medicationProvider = models.ForeignKey(Provider, on_delete=models.CASCADE,related_name='+', null = True)
    communicationProvider = models.ForeignKey(Provider, on_delete=models.CASCADE,related_name='+', null = True)
    activityProvider = models.ForeignKey(Provider, on_delete=models.CASCADE,related_name='+', null = True)
    mealProvider = models.ForeignKey(Provider, on_delete=models.CASCADE,related_name='+', null = True)

    def __str__(self):
        return self.firstName + " " +self.lastName

class Carer(models.Model):
    firstName = models.CharField(max_length=50)
    lastName = models.CharField(max_length=50)
    gender = models.CharField(max_length=1)
    password = models.CharField(max_length=128)
    email = models.CharField(max_length=128)
    phone = models.CharField(max_length=13)
    key = models.CharField(max_length=62) #62-bit key used to verify user login
    User = models.ForeignKey(User,on_delete=models.CASCADE,related_name='+', null = True)
    relation_code = models.CharField(max_length=10, default='1234567890')

    def __str__(self):
        return self.firstName + " " +self.lastName



class Tasks(models.Model):
    title = models.CharField(max_length=50)
    classTask = models.CharField(max_length=20, default="None")
    detail = models.TextField()
    create_date = models.DateField()
    start_date = models.DateField()
    Finish_date = models.DateField(null = True)
    user = models.ForeignKey("User",on_delete=models.CASCADE,related_name='+', null =True)
    carer = models.ForeignKey("Carer",on_delete=models.CASCADE,related_name='+', null =True)
    provider = models.ForeignKey("Provider",on_delete=models.CASCADE,related_name='+', null =True)
    status = models.IntegerField(default=0) # 1 = Finished this task; 0 = Not Finish this task
    comments = models.TextField(null = True)
    start_time = models.TimeField(null = True)
    end_time = models.TimeField(null = True)
    def __str__(self):
        return self.title


class List(models.Model):
    title = models.CharField(max_length=50)
    list_id = models.CharField(max_length=10, default='1234567890')
    User = models.ForeignKey(User,on_delete=models.CASCADE,related_name='+', null = True)


class Notes(models.Model):
    title = models.CharField(max_length=50)
    detail = models.TextField()
    create_date = models.DateField()
    start_date = models.DateField()
    Finish_date = models.DateField(null = True)
    user = models.ForeignKey("User",on_delete=models.CASCADE,related_name='+', null =True)
    carer = models.ForeignKey("Carer",on_delete=models.CASCADE,related_name='+', null =True)
    provider = models.ForeignKey("Provider",on_delete=models.CASCADE,related_name='+', null =True)
    status = models.IntegerField(default=0) # 1 = Finished this task; 0 = Not Finish this task
    start_time = models.TimeField(null = True)
    end_time = models.TimeField(null = True)
    def __str__(self):
        return self.title



import hashlib
import time

class key_generator(models.Model):

    def Generate_key(self, keystr):
        ''' Generate a UUID by the given key string. Key string: name, birthdate, register date.'''

        SHA1_str = hashlib.sha1(keystr.encode("utf-8"))
        encrypts = SHA1_str.hexdigest()
        return encrypts


    def Generate_time_key(self):
        ''' Generate a UUID by the accessing time. '''
        current_time = str(time.time())
        SHA1_str = hashlib.sha1(current_time.encode("utf-8"))
        encrypts = SHA1_str.hexdigest()
        return encrypts


    def Generate_combination_key(self,keystr):
        ''' Generate a 40-digits UUID conbining time and user key (name, birthdate, register date).
        Split the time key and user key into 4 segments respectively (10 digits per segment).
        The combined UUID will consist of: time seg4 + user key seg1 + time seg3 + user key seg 4 '''
        time_key = models.Generate_time_key(self)
        user_key = models.Generate_key(self,keystr)
        encrypt_str = time_key[30:40] + user_key[0:10] + time_key[20:30] + user_key[30:40]
        return encrypt_str
