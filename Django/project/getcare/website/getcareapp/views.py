from django.shortcuts import render, get_object_or_404
from django.http import HttpResponse, HttpResponseRedirect
from .models import *
from django.utils import timezone
from django.urls import reverse
import random
import string
from django.core.exceptions import ObjectDoesNotExist

def getCarerbyKey(currentKey):
    try:
        return Carer.objects.get(key = currentKey)
    except ObjectDoesNotExist:
        return False

def getUserbyKey(currentKey):
    try:
        return User.objects.get(key = currentKey)
    except ObjectDoesNotExist:
        return False

def getProviderbyKey(currentKey):
    try:
        return Provider.objects.get(key = currentKey)
    except ObjectDoesNotExist:
        return False

def findingUser(currentKey):
    current_user = getUserbyKey(currentKey)
    userType = "USER"
    if (current_user == False):
        current_user = getCarerbyKey(currentKey)
        userType = "CARER"
    if (current_user == False):
        current_user = getProviderbyKey(currentKey)
        userType = "PROVIDER"
    if (current_user == False):
        userType = "NONE"
    return current_user, userType

def findingTasks(currentUser, userType):
    user_tasks = ""
    if userType == "USER":
        user_tasks = Tasks.objects.filter(user = currentUser)
    elif userType == "CARER":
        user_tasks = Tasks.objects.filter(carer = currentUser)
    elif userType == "PROVIDER":
        user_tasks = Tasks.objects.filter(provider = currentUser)
    return user_tasks

def findingNotes(currentUser, userType):
    user_notes = ""
    if userType == "USER":
        user_tasks = Notes.objects.filter(user = currentUser)
    elif userType == "CARER":
        user_tasks = Notes.objects.filter(carer = currentUser)
    elif userType == "PROVIDER":
        user_tasks = Notes.objects.filter(provider = currentUser)
    return user_notes

def getUserbyEmail(currentEmail):
    try:
        return User.objects.get(email = currentEmail)
    except ObjectDoesNotExist:
        return False

def getCarerbyEmail(currentEmail):
    try:
        return Carer.objects.get(email = currentEmail)
    except ObjectDoesNotExist:
        return False

def getProviderbyEmail(currentEmail):
    try:
        return Provider.objects.get(email = currentEmail)
    except ObjectDoesNotExist:
        return False

def getTask(taskID, userKey):
    current_user, userType = findingUser(userKey)
    
    if userType == "USER":
        user_tasks = Tasks.objects.filter(user = current_user)
    elif userType == "CARER":
        user_tasks = Tasks.objects.filter(carer = current_user)
    elif userType == "PROVIDER":
        user_tasks = Tasks.objects.filter(provider = current_user)

    for user_task in user_tasks:
        if user_task.id == taskID:
            return Tasks.objects.get(id=taskID)
    return False

def getNote(noteID, userKey):
    current_user, userType = findingUser(userKey)
    
    if userType == "USER":
        user_notes = Notes.objects.filter(user = current_user)
    elif userType == "CARER":
        user_notes = Notes.objects.filter(carer = current_user)
    elif userType == "PROVIDER":
        user_notes = Notes.objects.filter(provider = current_user)

    for user_note in user_notes:
        if user_note.id == noteID:
            return Notes.objects.get(id=noteID)
    return False

def register(request):
    if request.method =="GET":
        return render(request,'GetCare/register.html')
    
    # random the key
    seed = string.ascii_letters + string.digits
    key = random.sample(seed,62)
    all_user = User.objects.all()
    i = 0
    key_list = []
    for i in range(len(all_user)):
        key_list.append(all_user[i].key)
    while key in key_list:
        key = random.sample(seed,62) 
    
    if request.POST['role'] == 'senoir':
        if request.POST['relationCode'] =='':
            return HttpResponse("Please enter your relation code")
        try:
            mycarer = Carer.objects.get(relation_code = request.POST['relationCode'])
        except ObjectDoesNotExist:
            return HttpResponse("We cannot find this relation code in our system")
        
        User.objects.create(firstName = request.POST['FirstName'], 
                            lastName = request.POST['LastName'], 
                            email = request.POST['email'], 
                            password = request.POST['password'], 
                            gender = request.POST['gender'], 
                            phone = request.POST['phone'], 
                            key = "".join(key),
                            carer = mycarer,
                            )
        mycarer.User = User.objects.get(email = request.POST['email'])
        mycarer.save()
        return HttpResponse(r"Successful.<br><a href=\getcare\login> Login </a>")
    elif request.POST['role'] == 'carer':
        relationCode = random.randint(0,999999)
        Carer.objects.create(firstName = request.POST['FirstName'], 
                            lastName = request.POST['LastName'], 
                            email = request.POST['email'], 
                            password = request.POST['password'], 
                            gender = request.POST['gender'], 
                            phone = request.POST['phone'], 
                            key = "".join(key),
                            relation_code = relationCode,)
        return HttpResponse("Successful. Your reflation code for senior is " + str(relationCode) + r"<br><a href=\getcare\login> Login </a>")
    elif request.POST['role'] == 'provider':
        relationCode = random.randint(0,999999)
        Provider.objects.create(firstName = request.POST['FirstName'], 
                            lastName = request.POST['LastName'], 
                            email = request.POST['email'], 
                            jobType = request.POST['jobType'], 
                            password = request.POST['password'], 
                            gender = request.POST['gender'], 
                            phone = request.POST['phone'], 
                            key = "".join(key),
                            relation_code = relationCode,)
        return HttpResponse("Successful. Your relation code for senior is " + str(relationCode) + r"<br><a href=\getcare\login> Login </a>")


def index(request, currentKey, classTask):
    
    # Finding current user by Key
    current_user, userType = findingUser(currentKey)
    if (current_user == False):
        return HttpResponse("Invalid key" + r"<br><a href=\getcare\login> Login </a>")
    # Loading current user Tasks
    User_tasks = findingTasks(current_user, userType)

    # Loading current user Notes.
    User_notes = findingNotes(current_user, userType)

    # Filting tasks which should be display
    result_tasks = []
    if classTask == "ALL" or classTask == "INCOMPLETED":
        for i in User_tasks:
            if i.status == 0:
                result_tasks.append(i)
    elif classTask == "COMPLETED":
        for i in User_tasks:
            if i.status == 1:
                result_tasks.append(i)
    else:            
        for i in User_tasks:
            if i.classTask == classTask and i.status == 0:
                result_tasks.append(i)

    result_notes = []    
    for i in User_notes:
        result_notes.append(i)


    if userType == "USER":
        return render(request,'GetCare/index.html',{'user': current_user,'Tasks':result_tasks})
    elif userType == "CARER":
        return render(request,'GetCare/index_carer.html',{'user': current_user,'Tasks':result_tasks})
    elif userType == "PROVIDER":
        return render(request,'GetCare/index_provider.html',{'user': current_user,'Tasks':result_tasks})
    else:
        return render(request,'GetCare/index.html',{'user': current_user,'Tasks':result_tasks})

    if userType == "USER":
        return render(request,'GetCare/index.html',{'user': current_user,'Notes':result_notes})
    elif userType == "CARER":
        return render(request,'GetCare/index_carer.html',{'user': current_user,'Notes':result_notes})
    elif userType == "PROVIDER":
        return render(request,'GetCare/index_provider.html',{'user': current_user,'Notes':result_notes})
    else:
        return render(request,'GetCare/index.html',{'user': current_user,'Notes': result_notes})



def login(request):
    if request.method == "GET":
        return render(request,'GetCare/login.html')

    currentEmail = request.POST.get('email')
    typePassword = request.POST.get('password')

    if request.POST.get('role') =='senoir':    
        current_user = getUserbyEmail(currentEmail)
        if current_user:
            if current_user.password == typePassword:
                return HttpResponseRedirect("/getcare/" + current_user.key + "/index/ALL")
    elif request.POST.get('role') =='carer':
        current_user = getCarerbyEmail(currentEmail)
        if current_user:
            if current_user.password == typePassword:
                return HttpResponseRedirect("/getcare/" + current_user.key + "/index/ALL")
    elif request.POST.get('role') =='provider':
        current_user = getProviderbyEmail(currentEmail)
        if current_user:
            if current_user.password == typePassword:
                return HttpResponseRedirect("/getcare/" + current_user.key + "/index/ALL")
    

    
    return HttpResponse(r"User information error.<br><a href=\getcare\login> Try agian </a>")


def newTask(request,currentKey):
    current_user, userType = findingUser(currentKey)
    if request.method == "GET":
        if userType == "USER":
            return render(request,'GetCare/newtask.html',{'user':current_user})
        elif userType == "CARER":
            return render(request,'GetCare/newtask_carer.html',{'user':current_user})
        elif userType == "PROVIDER":
            return render(request,'GetCare/newtask_provider.html',{'user':current_user})
        else:
            return render(request,'GetCare/newtask.html',{'user':current_user})

    currTitle = request.POST.get('title')
    CurrDate = request.POST.get('date')
    CurrDetail = request.POST.get('detail')
    classTask = request.POST.get('classTask')
    # CurrComment = request.POST.get('comments')
    # CurrTime = request.POST.get('time')
    myProvider = None

    if classTask == "ACTIVITY":
        myProvider = current_user.activityProvider
    elif classTask == "CAREPLAN":
        myProvider = current_user.careProvider
    elif classTask == "COMMUNICATION":
        myProvider = current_user.communicationProvider
    elif classTask == "MEAL":
        myProvider = current_user.mealProvider
    elif classTask == "MEDICATION":
        myProvider = current_user.medicationProvider

    if (userType == "USER"):
        Tasks.objects.create(title = currTitle, create_date = timezone.now(), detail = CurrDetail, start_date = CurrDate, user = current_user, classTask = classTask, provider = myProvider, carer = current_user.carer)
    elif (userType == "CARER"):
        Tasks.objects.create(title = currTitle, create_date = timezone.now(), detail = CurrDetail, start_date = CurrDate, carer = current_user, classTask = classTask)
    elif (userType =="PROVIDER"):
        Tasks.objects.create(title = currTitle, create_date = timezone.now(), detail = CurrDetail, start_date = CurrDate, provider = current_user, classTask = classTask)
    
    return HttpResponseRedirect("/getcare/" + current_user.key + "/index/ALL")

def viewtask(request,currentKey, task_id):
    if request.method == "GET":
        current_user, userType = findingUser(currentKey)
        if current_user:
            current_task = getTask(task_id,currentKey)
            if current_task:
                if userType == "USER":
                    return render(request,'GetCare/tasks.html',{'task':current_task,'user':current_user})
                elif userType == "CARER":
                    return render(request,'GetCare/tasks_carer.html',{'task':current_task,'user':current_user})
                elif userType == "PROVIDER":
                    return render(request,'GetCare/tasks_provider.html',{'task':current_task,'user':current_user})
                else:
                    return render(request,'GetCare/tasks.html',{'task':current_task,'user':current_user})
        else:
            return HttpResponse(r"User information error.<br><a href=\getcare\login> Try agian </a>")
    
    changeTask = getTask(task_id,currentKey)

    if changeTask:
        current_user, typeUser = findingUser(currentKey)
        if typeUser == "USER":
            User_tasks = Tasks.objects.filter(user = current_user)
        elif typeUser == "CARER":
            User_tasks = Tasks.objects.filter(carer = current_user)
        elif typeUser =="PROVIDER":
            User_tasks = Tasks.objects.filter(provider = current_user)

        changeTask.title = request.POST.get('title')
        changeTask.detail = request.POST.get('detail')
        changeTask.start_date = request.POST.get('date')
        changeTask.comments = request.POST.get('comments')
        changeTask.start_time = request.POST.get('time')
        if request.POST.get('finish') == "finished":
            changeTask.status = 1
        else:
            changeTask.status = 0
        changeTask.save()
        if request.POST.get('delete') == "deleted":
            changeTask.delete()
        
        return HttpResponseRedirect("/getcare/"+ str(currentKey) + "/index/ALL") 
    else:
        return HttpResponse(r"User information error.<br><a href=\getcare\login> Try agian </a>")

def info(request, currentKey):
    current_user, userType = findingUser(currentKey)
    if request.method == "GET":
        return render(request,'GetCare/info.html',{'user':current_user})
    
    current_user.firstName = request.POST.get('firstName')
    current_user.lastName = request.POST.get('lastName')
    current_user.email = request.POST.get('email')
    current_user.phone = request.POST.get('phone')
    if request.POST.get('password') != "":
         current_user.password = request.POST.get('password')

    try:
        current_user.carer = Carer.objects.get(relation_code = request.POST.get('carerCode'))
        current_user.careProvider = Provider.objects.get(relation_code = request.POST.get('careCode'))
        current_user.medicationProvider = Provider.objects.get(relation_code = request.POST.get('medicationCode'))
        current_user.communicationProvider = Provider.objects.get(relation_code = request.POST.get('communicationCode'))
        current_user.activityProvider = Provider.objects.get(relation_code = request.POST.get('activityCode'))
        current_user.mealProvider = Provider.objects.get(relation_code = request.POST.get('mealCode'))
    except:
        return HttpResponse(r"Your Relation Code is incorrect!")
    current_user.save()
    return HttpResponseRedirect("/getcare/"+ str(currentKey) + "/index/ALL") 


def newnote(request,currentKey):
    current_user, userType = findingUser(currentKey)
    if request.method == "GET":
        if userType == "USER":
            return render(request,'GetCare/newnote.html',{'user':current_user})
        elif userType == "CARER":
            return render(request,'GetCare/newnote_carer.html',{'user':current_user})
        elif userType == "PROVIDER":
            return render(request,'GetCare/newnote_provider.html',{'user':current_user})
        else:
            return render(request,'GetCare/newnote.html',{'user':current_user})

    currTitle = request.POST.get('title')
    CurrDate = request.POST.get('date')
    CurrDetail = request.POST.get('detail')
    myProvider = None

    if (userType == "USER"):
        Notes.objects.create(title = currTitle, create_date = timezone.now(), detail = CurrDetail, start_date = CurrDate, user = current_user, provider = myProvider, carer = current_user.carer)
    elif (userType == "CARER"):
        Notes.objects.create(title = currTitle, create_date = timezone.now(), detail = CurrDetail, start_date = CurrDate, carer = current_user)
    elif (userType =="PROVIDER"):
        Notes.objects.create(title = currTitle, create_date = timezone.now(), detail = CurrDetail, start_date = CurrDate, provider = current_user)
    
    return HttpResponseRedirect("/getcare/" + current_user.key + "/index/ALLnotes")


def viewnote(request,currentKey, note_id):
    if request.method == "GET":
        current_user, userType = findingUser(currentKey)
        if current_user:
            current_note = getNote(note_id,currentKey)
            if current_note:
                if userType == "USER":
                    return render(request,'GetCare/notes.html',{'note':current_note,'user':current_user})
                elif userType == "CARER":
                    return render(request,'GetCare/notes_carer.html',{'note':current_note,'user':current_user})
                elif userType == "PROVIDER":
                    return render(request,'GetCare/notes_provider.html',{'note':current_note,'user':current_user})
                else:
                    return render(request,'GetCare/notes.html',{'note':current_note,'user':current_user})
        else:
            return HttpResponse(r"User information error.<br><a href=\getcare\login> Try agian </a>")
    
    changeNote = getNote(note_id,currentKey)

    if changeNote:
        current_user, typeUser = findingUser(currentKey)
        if typeUser == "USER":
            User_notes = Notes.objects.filter(user = current_user)
        elif typeUser == "CARER":
            User_notes = Notes.objects.filter(carer = current_user)
        elif typeUser =="PROVIDER":
            User_notes = Notes.objects.filter(provider = current_user)

        changeNote.title = request.POST.get('title')
        changeNote.detail = request.POST.get('detail')
        changeNote.start_date = request.POST.get('date')
        changeNote.start_time = request.POST.get('time')
        # if request.POST.get('finish') == "finished":
        #     changeTask.status = 1
        # else:
        #     changeTask.status = 0
        # changeTask.save()
        if request.POST.get('delete') == "deleted":
            changeNote.delete()
        
        return HttpResponseRedirect("/getcare/"+ str(currentKey) + "/index/ALL") 
    else:
        return HttpResponse(r"User information error.<br><a href=\getcare\login> Try agian </a>")


