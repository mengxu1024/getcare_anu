from django.apps import AppConfig


class GetcareappConfig(AppConfig):
    name = 'getcareapp'
