from django.urls import path

from . import views

app_name = 'getcare'

urlpatterns = [
    path('',views.login, name='login'),  
    path('register/',views.register, name='register'),
    path('login/',views.login, name='login'),
    path('<str:currentKey>/index/<str:classTask>', views.index, name='index'),
    path('<str:currentKey>/newtask/', views.newTask, name='newtask'),
    path('<str:currentKey>/viewtask/<int:task_id>', views.viewtask, name='viewtask'),
    path('<str:currentKey>/info/', views.info, name='info'),
    path('<str:currentKey>/viewnote/<int:note_id>',views.viewnote, name='viewnote'),
    path('<str:currentKey>/newnote/', views.newnote, name='newnote'),
    

]