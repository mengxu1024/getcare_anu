from django.contrib import admin
from.models import *
# Register your models here.

admin.site.register(User)
admin.site.register(Tasks)
admin.site.register(Carer)
admin.site.register(Provider)

admin.site.register(Notes)