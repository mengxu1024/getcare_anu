# What is Getcare?
Getcare is a service which aims to facilitate the caretaking of elderly people by caretakers. It will achieve this by providing the means to communicate and coordinate the efforts of carers trying to care for their elderly loved ones. 

The service will allow a Carer to have visibility of all services and support being provided to the person being cared for in a calendar and schedule view (with ability to change appointments or request particular services for scheduled visits). Information presented will be linked directly to the source allowing dynamic updating (eg. the schedule of visits approved by an in-home support provider or weekly social activities the person being cared for is enrolled in). The Carer, or person being cared for, will be able to add personal activities to the calendar (eg doctor visits). Providers interacting with the person being cared for will be able to provide updates through the app (eg. completion of tasks assigned to an activity, ad-hoc notes, requests to the Carer).

Previous teams working on this project have already developed a prototype web application using Django. This semester’s team will focus on improving the prototype and developing support for mobile users, as well as sourcing feedback from user testers to improve the project later in the semester.
# Getcare Stakeholder Roles and Benefits

 #### Client (Stephen Blackburn)
- The client will interact with members of the development team their requirements for the project. At weekly meetings they will assess the state of the project and be given an oppourtunity to voice their suggestions and concerns with the project. At the conclusion of the semester they will have a working, user tested website with server hosting and mobile support.

 #### Members of the Project Development Team
- Members of the development team will attend weekly meetings and biweekly client meetings in which the state of the project and future tasks to be completed will be discussed. They will also attend workshops to listen to feedback from both tutors/mentors and members of the shadow team. Development team members will gain the experience of working in a team and in working to build an app and maintain a project.

 #### Techlauncher Assessment Team (Tutors/Mentors)
- The mentors will provide feedback about the current state of the project to the development team in the form of weekly workshops and assessment feedback (audits, etc).

 #### Shadow Team (Our Culture)
- The shadow team will provide verbal critique and suggestions throughout the semester during audits and weekly workshops. This will provide the development team with a different perspective and allow us to alter our ways of work to ensure a better result. The Shadow Team will also gain insight into our methods of work and will be able to learn from our experience in what are efficient and inefficient methods of working.

 #### User Testers
- When the project is in a test-ready state toward the end of the semester there may be an oppourtunity for selected users to act as beta testers. Users would be walked through the app and website by the development team, before providing feedback. This would allow us to create a 
list of functionalities that need improving in order to better meet user requirements. User testers would in turn receive a final product that will better suit their needs and be more user friendly for them.

# Project schedule
### Week 3 - Week 7
- Continue to develop the website until the broken functionalities outlined by last semesters team are fixed. Create mobile friendly web-app version

### Week 7 - Week 10
- Team members will begin the process of user testing and changing functionality based on user input

### Week 10 - The End of Semester
- If the website has been completed, including the mobile version then explore whether it is worthwhile creating a standalone java app
# Members
| Member           | UID          | Role                      | Spokesperson       |
| ---------------  | -------------|---------------------------|--------------------|
| Shixian Li       | u5794589     | Back-End                  | Yes                |
| Lachlan Barnes   | u6669624     | Back-End(Project Manager) | Yes                |
| Shimeng Ding     | u6213716     | Front-End                 | No                 |
| Xing Meng        | u6483085     | Back-End                  | No                 |
| Min Zhu          | u6642413     | Front-End                 | No                 |
| Tao Zhao         | u6201317     | Back-End                  | No                 |
| Wanqi Liao       | u6976221     | Back-End                  | No                 |

- Note that all members can be contacted via their ANU email
# Links
Trello: https://trello.com/b/9DBAkTek/getcare-anu-s1-2021

Google Drive:  https://drive.google.com/drive/folders/1zTDBExBUOH7_AsXnU7Sz1JLHDqgwzub7?usp=sharing

UI testing website: http://140.238.201.37/GetCareUI/


# S1(2021) Documents 


 - [Statement of Work](https://docs.google.com/document/d/1ykJrv-feGa6p9VkflKh6LUrlGzwZI-5qRQxT1PFeg_4/edit)
 - [Industry Project Student Agreement](https://drive.google.com/open?id=170t72dfRX-ydoAIMNuvRZ1KOSeNwSO9d)
 - [User Story Map](https://drive.google.com/file/d/16s_7RLuj802QFRPTvEj9EoJMnH1AZX1Q/view?usp=sharing)
 - [Decision making log](https://docs.google.com/spreadsheets/d/1WCXL1EnwVKquGYUWLYEjk-brlaRDXRSc/edit#gid=931183004)
 - [Feedback log](https://docs.google.com/spreadsheets/d/1Hb_OZhNcOVqiKQ1BzOiBJT48mqWAtndO223RxZs1xGg/edit?usp=sharing)
 - [Project schedule & sprint deliveries](https://drive.google.com/file/d/1i_PmIkrxYiXlbtlQX1iglfKYq_1XrbGY/view?usp=sharing)



# S1(2021) Weekly Agenda

**Week 2**
 - [Agenda Summary](https://docs.google.com/document/d/1Letmi49DmOzFZ-81mUg904m17ovYEVobadtVoklU9V0/edit?usp=sharing)
 - [Client Meeting](https://docs.google.com/document/d/1pXphffjBPDdUzL-TJ0q3jnfdDS-W2pB6E-iPd-wJohI/edit?usp=sharing)
 - [Team Meeting](https://docs.google.com/document/d/1-q7HU_2WsbA7UvL6YN0wv57fGSLnKqcnA-DADq3wOOM/edit)
 - [Descision Making](https://docs.google.com/document/d/11Wh3bh_uoyKtcY2QifvXzyKWiyVR4h0-A8DoCH7Hr68/edit)


**Week 3**
 - [Agenda Summary](https://docs.google.com/document/d/1WJCyHD771leOKCRwvRUSQtIn8-7vURQYfkKQFN6Jkgo/edit)
 - [Team Meeting](https://docs.google.com/document/d/1bAE0qaK2qK7_HghpUKBdNBXbgEH1HwpyxWXN5nopyiY/edit)
 - [Client Meeting](https://docs.google.com/document/d/1Nbz3xwmTViTt_BmVU4el_H-ZsmEvk1rbWQKVi15vhsw/edit)
 - [Descision Making](https://docs.google.com/document/d/1N-D1HqJvNbrkZf7fAhfGCTOXKDAIqdUNwv0Yqoc-MG8/edit)


 **Week 4** 
 - [Agenda Summary](https://docs.google.com/document/d/1uZtP0nJ2E7Kdg0_rHQCwTsDJGB-0DnkaPgPOoN2tt_U/edit)
 - [Team Meeting](https://docs.google.com/document/d/1JJzsfqusm0VK-d46O3jPZ08HKqUf7VUT2VbpXNqv424/edit)
 - [Client Meeting](https://docs.google.com/document/d/1dTbRmzas5T9I2BAzlw2j6w0DzYy8KTUqTZolQlf7xJ8/edit?usp=sharing)
 - [Descision Making](https://docs.google.com/document/d/1P6pmUmeVB2UC4QlWHiKnarIwcJqeRw-SqG_oa-8N_0k/edit)

  **Week 5** 
 - [Agenda Summary](https://docs.google.com/document/d/1fN7RYlIf13A_VQug-0J1LbRWXwvUUi1jIFj3NHUuEPk/edit?usp=sharing)
 - [Team Meeting](https://docs.google.com/document/d/1dCIE5L_xuBG-p9kKfzS3eJ-rAtVKDefKZR0QfJTvfxs/edit?usp=sharing)
 - [Client Meeting](https://docs.google.com/document/d/1z-WSuTgf4GnN63pxO-ZfIxjkfbK_Sv-45OuX7QC473s/edit?usp=sharing)
 

# S2(2020) Documents 

 - note: some of these links need backdating (they currently point to 2021 links when they shouldn't)
 - [Statement of Work](https://docs.google.com/document/d/1ykJrv-feGa6p9VkflKh6LUrlGzwZI-5qRQxT1PFeg_4/edit)
 - [Industry Project Student Agreement](https://drive.google.com/open?id=170t72dfRX-ydoAIMNuvRZ1KOSeNwSO9d)
 - [User Story Map](https://drive.google.com/file/d/16s_7RLuj802QFRPTvEj9EoJMnH1AZX1Q/view?usp=sharing)
 - [UML Design](https://drive.google.com/open?id=1kX8XjnH941UlrHdP0wwHXFprsz_gtN8x)
 - [UI design](https://drive.google.com/open?id=1Q7XvsHQQWQncrMCfMmKMR_cjbq-uUj-9)
 - [Decision making log](https://docs.google.com/spreadsheets/d/1a6iektYKlq4noZgkFNZqE1DHMvALZis9bkjkmfzA0tc/edit?usp=sharing)
 - [Feedback log](https://docs.google.com/spreadsheets/d/1J5j0q62HYfOWOTVh42cZBkoTs8bod3Mgi7_nJHYJW-Y/edit?usp=sharing)
 - [Project schedule & sprint deliveries](https://drive.google.com/file/d/1i_PmIkrxYiXlbtlQX1iglfKYq_1XrbGY/view?usp=sharing)

# S2 (2020) Weekly Agenda
**Week 2**
 - [Team Meetings](https://drive.google.com/file/d/1j5g_mo9ZDaQxa-qM52xCAjuWz1ldKTsr/view?usp=sharing)
 - [Decision Making](https://docs.google.com/spreadsheets/d/1a6iektYKlq4noZgkFNZqE1DHMvALZis9bkjkmfzA0tc/edit?usp=sharing)
 - [Client Meetings](https://drive.google.com/file/d/1Bw9VZgjLSGwa9jRtian3PS5yjLcXOaTK/view?usp=sharing)

**Week 3**
 - [Team Meetings](https://drive.google.com/file/d/1my1GFxG6p9MdQsWNbgoTiMrDaUUtCMX1/view?usp=sharing)
 - [Decision Making](https://docs.google.com/spreadsheets/d/1a6iektYKlq4noZgkFNZqE1DHMvALZis9bkjkmfzA0tc/edit?usp=sharing)

**Week 4**
 - [Team Meetings](https://drive.google.com/file/d/1xqRwnE87FT3HpsLQQt73IaO9S8Gb5hge/view?usp=sharing)
 - [Decision Making](https://docs.google.com/spreadsheets/d/1a6iektYKlq4noZgkFNZqE1DHMvALZis9bkjkmfzA0tc/edit?usp=sharing)
 - [Agenda](https://drive.google.com/file/d/1JiVlyS09RwbJMLuQcPB5p_Tamq-XsSQO/view?usp=sharing)

**Week 5**
 - [Team Meetings](https://drive.google.com/file/d/1TXyO2qtQ6mVPptK_2ZF0yXI8mDcteg2r/view?usp=sharing)
 - [Client Meetings](https://drive.google.com/file/d/1VXkXQ4NwPHVyeC3VGMV1dVPSBRCiKK0t/view?usp=sharing)
 - [Decision Making](https://docs.google.com/spreadsheets/d/1a6iektYKlq4noZgkFNZqE1DHMvALZis9bkjkmfzA0tc/edit?usp=sharing)
 - [Agenda](https://drive.google.com/file/d/1JiVlyS09RwbJMLuQcPB5p_Tamq-XsSQO/view?usp=sharing)
 
**Week 6**
 - [Team Meetings](https://drive.google.com/file/d/1GK2iUWHWCL4rE8YMdrxuoQ2mti82p6bG/view?usp=sharing)
 - [Decision Making](https://docs.google.com/spreadsheets/d/1a6iektYKlq4noZgkFNZqE1DHMvALZis9bkjkmfzA0tc/edit?usp=sharing)
 
**Week 7**
 - [Decision Making](https://docs.google.com/spreadsheets/d/1a6iektYKlq4noZgkFNZqE1DHMvALZis9bkjkmfzA0tc/edit?usp=sharing)

**Week 8**
 - [Decision Making](https://docs.google.com/spreadsheets/d/1a6iektYKlq4noZgkFNZqE1DHMvALZis9bkjkmfzA0tc/edit?usp=sharing)
 - [Client Meetings](https://drive.google.com/file/d/1uHtJukJ5c1ka_BVYp2GHKrNAattJx42c/view?usp=sharing)
 - [Team Meetings](https://drive.google.com/file/d/1Cicy0KxIpe6UaZlX6PHvgnNBCPd8IHdK/view?usp=sharing)


**Week 9**
 - [Decision Making](https://docs.google.com/spreadsheets/d/1a6iektYKlq4noZgkFNZqE1DHMvALZis9bkjkmfzA0tc/edit?usp=sharing)
 - [Team Meetings](https://drive.google.com/file/d/1lTp6568N6GF3fPHitCUMTUb4Ol3RWpSb/view?usp=sharing)
 - [Client Meetings](https://drive.google.com/file/d/1kvW1XeFXe4eU9L3m9C8T4iqZYnkEZam6/view?usp=sharing)



**Week 10**
 - [Decision Making](https://docs.google.com/spreadsheets/d/1a6iektYKlq4noZgkFNZqE1DHMvALZis9bkjkmfzA0tc/edit?usp=sharing)
 - [Team Meetings](https://drive.google.com/file/d/1TP1k88F1NCTAQAWETgyQtNZG5Vd36IbQ/view?usp=sharing)





